package com.epam.service;

import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.web.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;

public class UserServiceTest {

    UserDAO userDAO= Mockito.mock(UserDAO.class);

    BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();

    UserService userService=new UserService(userDAO,bCryptPasswordEncoder);

    @Test
    public void testGetUsers() {
        BDDMockito.when(this.userDAO.getCount()).thenReturn(2);

        BDDMockito.when(this.userDAO.getUsers(anyString(),anyInt(),anyInt())).thenReturn(null);

        Model model=Mockito.mock(Model.class);

        Assert.assertEquals("jsp/view/listOfUsers",userService.get(1,"default",model));

    }

    @Test
    public void testUserLogOut(){
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        MockHttpSession httpSession=new MockHttpSession();
        httpSession.setAttribute("user",user);
        Assert.assertEquals("redirect:/",userService.logOut(httpSession));
        Assert.assertNull(httpSession.getAttribute("user"));
    }


    @Test
    public void testUpdateUserRole() {
        User user=new User();
        user.setId((long)2);
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserRole(user);
        Assert.assertEquals("redirect:/users?orderBy=default&currentPage=1",
                userService.updateRole(user.getId(),1,"default"));

        Assert.assertEquals("redirect:/users?orderBy=default&currentPage=2",
                userService.updateRole(user.getId(),2,"default"));

        Assert.assertEquals("redirect:/users?orderBy=topicAsc&currentPage=1",
                userService.updateRole(user.getId(),1,"topicAsc"));

        Assert.assertEquals("redirect:/users?orderBy=topicAsc&currentPage=2",
                userService.updateRole(user.getId(),2,"topicAsc"));

    }

    @Test
    public void testUpdateUserMoney() {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        MockHttpSession mockHttpSession=new MockHttpSession();
        mockHttpSession.setAttribute("user",user);

        Assert.assertEquals("redirect:/expositionList?currentPage=1&sortBy=default",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "expositions","1","default",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/expositionList?currentPage=2&sortBy=default",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "expositions","2","default",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/expositionList?currentPage=1&sortBy=topicAsc",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "expositions","1","default",
                        "topicAsc",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/expositionList?currentPage=2&sortBy=topicAsc",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "expositions","2","default",
                        "topicAsc",user,Mockito.mock(Model.class),mockHttpSession));
    }

    @Test
    public void testUpdateUserMoneyWhileOnOrders() {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        MockHttpSession mockHttpSession=new MockHttpSession();
        mockHttpSession.setAttribute("user",user);

        Assert.assertEquals("redirect:/orders?currentPage=1&orderBy=default",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "orders","1","default",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/orders?currentPage=2&orderBy=default",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "orders","2","default",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/orders?currentPage=1&orderBy=topicAsc",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "orders","1","topicAsc",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/orders?currentPage=2&orderBy=topicAsc",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "orders","2","topicAsc",
                        "default",user,Mockito.mock(Model.class),mockHttpSession));

        Assert.assertEquals("redirect:/orders?currentPage=2&orderBy=topicAsc",
                userService.updateMoney("300","9999999999999999",
                        "404","user","1","20",
                        "orders","2","topicAsc",
                        "topicDesc",user,Mockito.mock(Model.class),mockHttpSession));
    }


    @Test
    public void testUserRegister() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin("existuser")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("existuser@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);

        Assert.assertEquals("redirect:/expositionList",
                userService.register("newuser@gmail.com", "password",
                        "newuser",
                        "password",
                        Mockito.mock(Model.class)));
    }
    @Test
    public void testUserRegisterEmailAndLoginExist() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin("existuser")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("existuser@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("existuser@gmail.com", "password",
                        "existuser",
                        "password",
                        Mockito.mock(Model.class)));
    }
    @Test
    public void testUserRegisterEmailExist() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin("existuser")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("existuser@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("existuser@gmail.com", "password",
                        "newuser",
                        "password",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterLoginExist() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin("existuser")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("existuser@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("newuser@gmail.com", "password",
                        "existuser",
                        "password",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterConfirmPassword(){
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin("existuser")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("existuser@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("newuser@gmail.com","password",
                        "newuser",
                        "nopassword",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterEmptyEmail() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(true);

        Assert.assertEquals("jsp/view/errorPage",
                userService.register("", "password",
                        "existuser@gmail.com",
                        "password",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterEmptyPassword() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(true);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("existuser@gmail.com", "",
                        "newuser",
                        "password",
                        Mockito.mock(Model.class)));
    }
    @Test
    public void testUserRegisterEmptyLogin() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(true);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("newuser@gmail.com", "password",
                        "",
                        "password",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterEmptyPasswordConfirm() {
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(true);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("newuser@gmail.com", "password",
                        "newuser",
                        "",
                        Mockito.mock(Model.class)));
    }

    @Test
    public void testUserRegisterInvalidEmail(){
        BDDMockito.doNothing().when(userDAO).add(any(User.class));
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(true);
        Assert.assertEquals("jsp/view/errorPage",
                userService.register("existuserasdsdsd","password",
                        "newuser",
                        "password",
                        Mockito.mock(Model.class)));
    }

}
