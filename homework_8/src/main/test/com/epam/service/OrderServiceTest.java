package com.epam.service;

import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.OrderDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import com.epam.entity.Order;
import com.epam.entity.User;
import com.epam.util.OrderQuery;
import com.epam.web.service.OrderService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class OrderServiceTest {

    UserDAO userDAO= Mockito.mock(UserDAO.class);

    OrderDAO orderDAO=Mockito.mock(OrderDAO.class);

    ExpositionDAO expositionDAO=Mockito.mock(ExpositionDAO.class);

    LocaleDAO localeDAO=Mockito.mock(LocaleDAO.class);

    OrderService orderService=new OrderService(userDAO,orderDAO,expositionDAO,localeDAO);


    @Test
    public void testGetOrders() {

        User user=new User();
        user.setId((long)1);
        BDDMockito.when(orderDAO.getCount(user)).thenReturn(2);
        String query= OrderQuery.get("default");

        Order order=new Order();
        order.setId((long)1);

        Order order1=new Order();
        order1.setId((long)2);

        MockHttpSession mockHttpSession=new MockHttpSession();

        mockHttpSession.setAttribute("user",user);
        mockHttpSession.setAttribute("defaultLocale","en");

        List<Order> orders= Arrays.asList(order,order1);

        BDDMockito.when(orderDAO.getUserOrders(user,"en",1,6,query)).thenReturn(orders);

        Model model=Mockito.mock(Model.class);

        Assert.assertEquals("jsp/view/bucket",
                orderService.getOrders(model,mockHttpSession,1,"default",user));
    }


    @Test
    public void testDeleteOrder()  {
        BDDMockito.doNothing().when(expositionDAO).delete(any(Exposition.class));
        MockHttpSession mockHttpSession=new MockHttpSession();
        User user=new User();
        user.setId((long)1);
        mockHttpSession.setAttribute("user",user);
        mockHttpSession.setAttribute("defaultLocale","en");

        Assert.assertEquals("redirect:/orders?orderBy=default&currentPage=1",
                orderService.delete(1,1,"default",mockHttpSession,user));

        Assert.assertEquals("redirect:/orders?orderBy=default&currentPage=2",
                orderService.delete(2,1,"default",mockHttpSession,user));

        Assert.assertEquals("redirect:/orders?orderBy=topicAsc&currentPage=1",
                orderService.delete(1,1,"topicAsc",mockHttpSession,user));

        Assert.assertEquals("redirect:/orders?orderBy=topicAsc&currentPage=2",
                orderService.delete(2,1,"topicAsc",mockHttpSession,user));

    }

    @Test
    public void testAddOrder(){

        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal("300"));
        user.setPassword("user");
        user.setEmail("user@gmail.com");
        user.setLogin("user");
        user.setRole("ROLE_USER");

        User user1=new User();
        user1.setId((long)2);
        user1.setMoney(new BigDecimal("0"));
        user1.setPassword("user");
        user1.setEmail("user@gmail.com");
        user1.setLogin("user");
        user1.setRole("ROLE_USER");

        Exposition exposition=new Exposition();

        exposition.setId((long)1);
        exposition.setCost(new BigDecimal("100"));
        exposition.setDate(java.sql.Date.valueOf(LocalDate.now()));
        exposition.setTicketsNum(3);
        exposition.setTopic("topic");

        Hall hall=new Hall();
        hall.setId((long)1);
        Hall hall1=new Hall();
        hall1.setId((long)2);

        exposition.getHalls().addAll(Arrays.asList(hall,hall1));
        exposition.setVisits(0);

        BDDMockito.when(localeDAO.getIdLocale("en")).thenReturn(1);

        BDDMockito.when(userDAO.getByID((long)2)).thenReturn(user1);

        BDDMockito.when(expositionDAO.getByID(exposition.getId(),1)).thenReturn(exposition);

        MockHttpSession mockHttpSession=new MockHttpSession();

        mockHttpSession.setAttribute("user",user);
        mockHttpSession.setAttribute("defaultLocale","en");

        Model model=Mockito.mock(Model.class);


        MockHttpSession mockHttpSession1=new MockHttpSession();

        mockHttpSession1.setAttribute("user",user1);
        mockHttpSession1.setAttribute("defaultLocale","en");

        Assert.assertEquals("redirect:/expositionList?sortBy=default&currentPage=1",
                orderService.buy(model,mockHttpSession,"default",1,1,1,user));

        Assert.assertEquals("redirect:/expositionList?sortBy=default&currentPage=2",
                orderService.buy(model,mockHttpSession,"default",1,2,1,user));

        Assert.assertEquals("redirect:/expositionList?sortBy=topicAsc&currentPage=1",
                orderService.buy(model,mockHttpSession,"topicAsc",1,1,1,user));

        Assert.assertEquals("redirect:/expositionList?sortBy=topicAsc&currentPage=2",
                orderService.buy(model,mockHttpSession,"topicAsc",1,2,1,user));

        Assert.assertEquals("jsp/view/errorPage",
                orderService.buy(model,mockHttpSession1,"default",1,1,1,user1));
    }

}
