package com.epam.controller;

import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.util.UserQuery;
import com.epam.web.controller.ErrorController;
import com.epam.web.controller.UserController;

import com.epam.web.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyInt;

public class UserControllerTest {

    UserDAO userDAO=Mockito.mock(UserDAO.class);

    BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();

    UserService userService=new UserService(userDAO,bCryptPasswordEncoder);

    private final UserController userController=new UserController(userService);

    private final MockMvc mockMvc= MockMvcBuilders
            .standaloneSetup(userController)
            .setControllerAdvice(new ErrorController())
            .alwaysDo(print()).build();

    @Test
    public void testUpdateUserMoney() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
       BDDMockito.doNothing()
               .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=1&sortBy=default"));
    }

    @Test
    public void testUpdateUserMoneyAnotherPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentPage","2")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=2&sortBy=default"));
    }


    @Test
    public void testUpdateUserMoneyAnotherSort() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("sortBy","topicAsc")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=1&sortBy=topicAsc"));
    }

    @Test
    public void testUpdateUserMoneyAnotherSortAndPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentPage","2")
                .param("sortBy","topicAsc")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=2&sortBy=topicAsc"));
    }


    @Test
    public void testUpdateUserMoneyAnotherView() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentView","orders")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=1&orderBy=default"));
    }

    @Test
    public void testUpdateUserMoneyAnotherViewWithAnotherOrder() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentView","orders")
                .param("orderBy","topicAsc")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=1&orderBy=topicAsc"));
    }

    @Test
    public void testUpdateUserMoneyAnotherViewWithAnotherPageAndAnotherOrder() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentView","orders")
                .param("currentPage","2")
                .param("orderBy","topicAsc")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=2&orderBy=topicAsc"));
    }

    @Test
    public void testUpdateUserMoneyAnotherViewWithAnotherPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentView","orders")
                .param("currentPage","2")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=2&orderBy=default"));
    }

    @Test
    public void testUpdateUserMoneyWithInvalidCardNumber() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","99")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    public void testUpdateUserMoneyWithOutCardNumber() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserMoneyWithOutCVV() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserMoneyWithOutCardHolderName() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserMoneyWithOutMoneyValue() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserMoneyWithOutMM() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserMoneyWithOutYY() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserMoney(user,new BigDecimal("300"));

        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));

    }

    @Test
    public void testUpdateUserRole() throws Exception {

        User user=new User();
        user.setId((long)2);
        BDDMockito.doNothing()
                .when(this.userDAO).updateUserRole(user);

        mockMvc.perform(post("/update")
                .param("user","2"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users?orderBy=default&currentPage=1"));

        mockMvc.perform(post("/update")
                .param("user","2")
                .param("orderBy","topicAsc"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users?orderBy=topicAsc&currentPage=1"));

        mockMvc.perform(post("/update")
                .param("user","2")
                .param("currentPage","2"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users?orderBy=default&currentPage=2"));

        mockMvc.perform(post("/update")
                .param("user","2")
                .param("orderBy","topicAsc")
                .param("currentPage","2"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users?orderBy=topicAsc&currentPage=2"));
    }

    @Test
    public void testGetUsers() throws Exception {

        BDDMockito.when(this.userDAO.getCount()).thenReturn(2);

        List<User>users=new ArrayList<>();

        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal("300"));
        user.setPassword("user");
        user.setEmail("user@gmail.com");
        user.setLogin("user");
        user.setRole("ROLE_USER");

        User user1 =new User();
        user1.setId((long)1);
        user1.setMoney(new BigDecimal("0"));
        user1.setPassword("admin");
        user1.setEmail("admin@gmail.com");
        user1.setLogin("admin");
        user1.setRole("ROLE_ADMIN");
        users.add(user);
        users.add(user1);

        BDDMockito.when(this.userDAO.getUsers(anyString(),anyInt(),anyInt())).thenReturn(users);

        mockMvc.perform(post("/users")).andExpect(status().
                isOk()).
                andExpect(model().
                        attributeExists("userList","orderBy","currentPage","noOfPages")).
                andExpect(view().name("jsp/view/listOfUsers"))
                .andExpect(model().attribute("userList",users));
    }

    @Test
    public void testUserRegister() throws Exception {
        BDDMockito.when(userDAO.getByLogin("userexist")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("userexist@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);

        mockMvc.perform(post("/register")
                .param("login", "newuser")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "newuser@gmail.com"))
                .andExpect(status()
                        .is3xxRedirection()).andExpect(view().name("redirect:/expositionList"));
    }
        @Test
        public void testUserRegisterEmailExist() throws Exception {
            BDDMockito.when(userDAO.getByLogin("userexist")).thenReturn(true);
            BDDMockito.when(userDAO.getByEmail("userexist@gmail.com")).thenReturn(true);
            BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
            BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
            mockMvc.perform(post("/register")
                    .param("login", "newuser")
                    .param("password", "password")
                    .param("password_confirm", "password")
                    .param("email", "userexist@gmail.com"))
                    .andExpect(status()
                            .isOk())
                    .andExpect(view().name("jsp/view/errorPage"))
                    .andExpect(model().attribute("errorMessage", "email.login.exist"));
        }
    @Test
    public void testUserRegisterLoginExist() throws Exception {
        BDDMockito.when(userDAO.getByLogin("userexist")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("userexist@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login", "userexist")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "newuser@gmail.com"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.exist"));
    }
    @Test
    public void testUserRegisterEmailAndLoginExist() throws Exception {
        BDDMockito.when(userDAO.getByLogin("userexist")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("userexist@gmail.com")).thenReturn(true);
        BDDMockito.when(userDAO.getByEmail("newuser@gmail.com")).thenReturn(false);
        BDDMockito.when(userDAO.getByLogin("newuser")).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login","userexist")
                .param("password","password")
                .param("password_confirm","password")
                .param("email","userexist@gmail.com"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage","email.login.exist"));
    }

    @Test
    public void testUserRegisterNoLogin() throws Exception {
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);

        mockMvc.perform(post("/register")
                .param("login", "")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "newuser@gmail.com"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }
        @Test
        public void testUserRegisterNoPassword() throws Exception {
            BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
            BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);
            mockMvc.perform(post("/register")
                    .param("login", "newuser")
                    .param("password", "")
                    .param("password_confirm", "password")
                    .param("email", "userexist@gmail.com"))
                    .andExpect(status()
                            .isOk())
                    .andExpect(view().name("jsp/view/errorPage"))
                    .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
        }

    @Test
    public void testUserRegisterNoConfirmPassword() throws Exception {
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login", "userexist")
                .param("password", "password")
                .param("password_confirm", "")
                .param("email", "newuser@gmail.com"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }

    @Test
    public void testUserRegisterNoEmail() throws Exception {
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login", "userexist")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", ""))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }
    @Test
    public void testUserRegisterConfirmPassword() throws Exception {
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login", "newuser")
                .param("password", "password")
                .param("password_confirm", "nopassword")
                .param("email", "userexist@gmail.com"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "password.confirm"));
    }
    @Test
    public void testUserRegisterEmailInvalid() throws Exception {
        BDDMockito.when(userDAO.getByLogin(anyString())).thenReturn(false);
        BDDMockito.when(userDAO.getByEmail(anyString())).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("login","newuser")
                .param("password","password")
                .param("password_confirm","password")
                .param("email","userexistdsadasd"))
                .andExpect(status()
                        .isOk())
                .andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage","email.invalid"));
    }

    @Test
    public void testUserLogOut() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        MockHttpSession httpSession=new MockHttpSession();
        httpSession.setAttribute("user",user);
        mockMvc.perform(post("/logOut").session(httpSession)
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view()
                        .name("redirect:/"));
        Assert.assertNull(httpSession.getAttribute("user"));
    }



}
