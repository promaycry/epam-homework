package com.epam.web.handler;

import com.epam.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@Component
public class SuccesLoginHandler implements AuthenticationSuccessHandler {

   @Autowired
   UserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        String login=httpServletRequest.getParameter("login");
        HttpSession session=httpServletRequest.getSession();
        userService.logIn(login,session);
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/expositionList");
    }

}
