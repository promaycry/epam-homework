package com.epam.web.service;


import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.OrderDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Order;
import com.epam.entity.User;
import com.epam.util.OrderQuery;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

@Service
public class OrderService extends MyService{

    private static final long serialVersionUID = -1101976618667657267L;

    private static final Logger log = Logger.getLogger(OrderService.class);

    private static final Integer ELEMENTS =6;

    UserDAO userDAO;

    OrderDAO orderDAO;


    ExpositionDAO expositionDAO;

    LocaleDAO localeDAO;

    @Autowired
    public OrderService(UserDAO userDAO,OrderDAO orderDAO,ExpositionDAO expositionDAO,LocaleDAO localeDAO){
        this.orderDAO=orderDAO;
        this.expositionDAO=expositionDAO;
        this.userDAO=userDAO;
        this.localeDAO=localeDAO;
    }

    public String getOrders(Model model, HttpSession session, int currentPage, String orderBy,User user){
        String locale= (String) session.getAttribute("defaultLocale");

        String query= OrderQuery.get(orderBy);

        log.trace("sort by--->"+orderBy);

        log.trace("sql query--->"+query);

        List<Order> orderList=orderDAO.getUserOrders(user,locale,currentPage,ELEMENTS,query);

        log.trace("orders-->"+orderList);


        model.addAttribute("orderList",orderList);

        log.trace("orderList-->"+orderList);

        model.addAttribute("currentPage",currentPage);
        log.trace("currentPage set --->"+currentPage);

        int count=orderDAO.getCount(user);
        log.trace("count of orders --->"+count);


        int numberOfPages=count/ ELEMENTS;
        log.trace("number of pages--->"+numberOfPages);

        if(count% ELEMENTS !=0){
            numberOfPages++;
            log.trace("number of pages--->"+numberOfPages);
        }

        model.addAttribute("noOfPages",numberOfPages);
        log.trace("number of pages sended--->"+numberOfPages);

        model.addAttribute("currentView","orders");
        log.trace("set currentView-->orders");

        model.addAttribute("orderBy",orderBy);
        log.trace("set orderBy->"+orderBy);

        return "jsp/view/bucket";
    }

    public String buy(Model model, HttpSession session, String orderBy, int id, int curPage, int value,User user){
        log.debug("Command buyExposition starts");
        Exposition exposition=new Exposition();

        BigDecimal money=user.getMoney();

        log.trace("user current-->"+user.getMoney());

        BigDecimal cost=expositionDAO.getByID(Long.valueOf(id),localeDAO.
                getIdLocale((String)session.getAttribute("defaultLocale"))).getCost();

        cost=cost.multiply(new BigDecimal(value));

        log.trace("cost-->"+cost);

        if(cost.compareTo(money)>0){
            String error ="priceToHigh";
            model.addAttribute("error",error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }

        Integer integer=Integer.valueOf(value);
        exposition.setId((long)id);
        log.trace("Initialize exposition with id--->"+exposition.getId());
        log.trace("Initialize number of tickets"+integer);
        orderDAO.add(user,exposition,integer);
        user=userDAO.getByID(user.getId());
        session.setAttribute("user",user);
        log.trace("user update-->"+user);
        log.debug("Order added");
        return "redirect:/expositionList?sortBy="+orderBy+"&currentPage="+curPage;
    }

    public String delete(int currentPage, long id, String sort, HttpSession session,User user){
        log.debug("Command start");
        Order order=new Order();
        order.setId(Long.valueOf(id));
        log.trace("order id set-->"+order.getId());
        orderDAO.delete(order);
        log.trace("order deleted id-->"+order.getId());
        user=userDAO.getByID(user.getId());
        session.setAttribute("user",user);
        log.trace("user update-->"+user);
        return"redirect:/orders?orderBy="+sort+"&currentPage="+currentPage;
    }

}
