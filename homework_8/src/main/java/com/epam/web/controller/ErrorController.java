package com.epam.web.controller;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(NoHandlerFoundException.class)
    public String exeption404() {
        return "/jsp/view/404";
    }



    public String error() {
        return "/jsp/view/errorPage";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String bad() {
        return "/jsp/view/errorPage";
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public String exeption() {
        return "/jsp/view/404";
    }

}
