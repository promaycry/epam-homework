package com.epam.web.controller;


import com.epam.annotation.Time;
import com.epam.entity.User;
import com.epam.web.service.OrderService;
import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class OrderController {

    OrderService orderService;


    @Autowired
    public OrderController(OrderService orderService){
        this.orderService=orderService;
    }

    @Time
    @RequestMapping(value = "/orders", method = {RequestMethod.GET, RequestMethod.POST})
    public String getOrders(Model model,
                            @SessionAttribute(name="user") User user,
                            HttpSession session,
                            @RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                            @RequestParam(name = "orderBy",defaultValue = "default")String orderBy
                            ){
        return orderService.getOrders(model,session,currentPage,orderBy,user);
    }

    @Time
    @RequestMapping(value = "/buy",method = {RequestMethod.POST})
    public String buy(Model model,
                      @SessionAttribute(name="user") User user,
                      HttpSession session,
                      @RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                      @RequestParam(name = "orderBy",defaultValue = "default")String orderBy,
                      @RequestParam(name = "exposition_id")int id,
                      @RequestParam(name = "number")int value){
        return orderService.buy(model,session,orderBy,id,currentPage,value,user);
    }

    @Time
    @RequestMapping(value = "/deleteOrder",method = {RequestMethod.POST})
    public String delete(HttpSession session,
                         @SessionAttribute(name="user") User user,
                         @RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                         @RequestParam(name = "orderBy",defaultValue = "default")String orderBy,
                         @RequestParam(name = "order")long id){
        return orderService.delete(currentPage,id,orderBy,session,user);
    }

}
