package com.epam.web.controller;

import com.epam.annotation.Time;
import com.epam.dao.HallDAO;
import com.epam.web.service.HomeService;
import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

	@Autowired
	SettingService settingService;

	@Autowired
	HomeService homeService;

	@Autowired
	HallDAO hallDAO;

	@RequestMapping("/")
	@Time
	public String getIndex(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		return homeService.getIndex(model,request,response,session,settingService);
	}

	@Time
	@RequestMapping(value = {"/registerPage"},method = {RequestMethod.GET, RequestMethod.POST})
	public String register(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		settingService.setLocale(response,request,session);
		model.addAttribute("currentView","register");
		return "jsp/view/registerPage";
	}

	@Time
	@RequestMapping(value = {"/addExposition"},method = {RequestMethod.GET, RequestMethod.POST})
	public String addExposition(Model model){
		model.addAttribute("currentView","addPage");
		model.addAttribute("Halls",hallDAO.getHalls());
		return "jsp/view/addExposition";
	}

}
