package com.epam.web.controller;


import com.epam.annotation.Time;
import com.epam.web.service.ExpositionService;
import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class ExpositionController {

    ExpositionService expositionService;

    @Autowired
    public ExpositionController(ExpositionService expositionService){
        this.expositionService=expositionService;
    }


    @Time
    @RequestMapping(value = "/expositionList", method = {RequestMethod.GET, RequestMethod.POST})
    public String expositionList(@RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                                 Model model,
                                 @RequestParam(name = "sortBy",defaultValue = "default")String orderBy,
                                 HttpSession session,
                                 HttpServletRequest request, HttpServletResponse response){
        return expositionService.get(session,currentPage,orderBy,model);
    }

    @Time
    @RequestMapping(value = "/add")
    public String addExposition(HttpServletRequest request, HttpServletResponse response, @RequestParam("image") MultipartFile file) throws IOException {
        return expositionService.add(response,request,file);
    }

    @Time
    @RequestMapping(value = "/delete")
    public String deleteExposition(@RequestParam(name = "sortBy",defaultValue = "default")String orderBy,
                                   @RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                                   @RequestParam(name="exposition_id") int id
                                   ){
        return expositionService.deleteExposition(id,currentPage,orderBy);
    }

}
