package com.epam.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * DB manager. Works with MySQl data base!.
 * Only the required DAO methods are defined!
 *
 * @author Kaliuha Aleksei
 *
 */

@Service
public class DBManager {

    @Autowired
    DataSource dataSource;

    private static DBManager instance;

    private DBManager() { }

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }



    public static void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con
     *            Connection to be rollbacked and closed.
     */
    public static void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }



    public Connection getConnection() throws SQLException {
        Connection connection=dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }



}
