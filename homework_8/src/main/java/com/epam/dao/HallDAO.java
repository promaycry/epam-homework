package com.epam.dao;

import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.dao.DBManager.commitAndClose;
import static com.epam.dao.DBManager.rollbackAndClose;


/**
 * Data access object for hall entity.
 */

@Repository
public class HallDAO {

    private static final String SQL_GET_HALLS_FOR_EXPOSITION=
            "Select hall_id from exposition_halls where exposition_id=?";

    private static final String SQL_GET_HALLS="Select id from halls";

    private static final String SQL_ADD_EXPOSITION_HALL="Insert into exposition_halls(exposition_id,hall_id) values(?,?)";

    @Autowired
    DBManager dbManager;


    /**
     * Returns all exposition halls.
     *
     *  @param exposition
     *
     * @return List of menu item entities.
     */
    public List<Hall> getExpositionsHalls(Exposition exposition){
        List<Hall>halls=new ArrayList<>();
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            Connection con = null;
            try {
                con = dbManager.getConnection();
                pstmt = con.prepareStatement(SQL_GET_HALLS_FOR_EXPOSITION);
                pstmt.setLong(1, exposition.getId());
                rs = pstmt.executeQuery();
                while (rs.next()){
                    halls.add(new Hall(rs.getLong(Fields.HALL_ID)));
                }
                rs.close();
                pstmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                rollbackAndClose(con);
            } finally {
                commitAndClose(con);
            }
        return halls;
    }

    /**
     * Returns all halls.
     *
     * @return List of halls.
     */
    public List<Hall>getHalls(){
        List<Hall>halls=new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_HALLS);
            rs = pstmt.executeQuery();
            while (rs.next()){
                halls.add(new Hall(rs.getLong(Fields.ID)));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return halls;
    }


    /**
     * Insert and connect halls to exposition
     *
     * @param id id of exposition
     *
     * @param hall id of hall
     *
     */
    public void insertHall(int id,Long hall){
        Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_ADD_EXPOSITION_HALL);
            pstmt.setInt(1,id);
            pstmt.setLong(2,hall);
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
        finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }
}
