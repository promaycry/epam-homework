package com.epam.dao;

import com.epam.dao.mapper.UserMapper;
import com.epam.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.dao.DBManager.commitAndClose;
import static com.epam.dao.DBManager.rollbackAndClose;


/**
 * Data access object for user entity.
 */

@Repository
public class UserDAO{

    private static final String SQL_GET_USER="Select * from users where login=? and password=?";

    private static final String SQL_GET_USER_BY_ID="Select * from users where id=?";

    private static final String SQL_GET_USER_BY_LOGIN="Select * from users where login=?";

    private static final String SQL_GET_USER_BY_EMAIL="Select * from users where email=?";

    private static final String SQL_INSERT_USER="INSERT INTO users(login,password,role_id,email) VALUES(?,?,?,?)";

    private static final String SQL_GET_USER_ROLE_ID="Select id from users_roles where role_name=?";

    private static final String SQL_GET_USER_ROLE="Select role_name from users_roles where id=?";

    private static final String SQL_UPDATE_USER_MONEY="Update users set money=money+? where id=?";

    private static final String SQL_UPDATE_USER_PASSWORD="Update users set password=? where id=?";

    private static final String SQL_UPDATE_USER_EMAIL="Update users set email=? where id=?";

    private static final String SQL_UPDATE_USER_ROLE="Update users set role_id=1 where id=?";

    private static final String SQL_GET_USERS_COUNT = "Select count(id) from users";

    @Autowired
    DBManager dbManager;

    @Autowired
    UserMapper userMapper;


    @Autowired
    DataSource dataSource;
    /**
     * Get user role type by String
     *
     * @param role String value of role
     *
     * @return int number value of role
     *
     */
    private int getId(String role){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int i=0;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_ROLE_ID);
            pstmt.setString(1, role);
            rs = pstmt.executeQuery();
            if (rs.next())
                i=rs.getInt(Fields.ID);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return i;
    }


    /**
     * Get users
     *
     *
     * @return users list
     *
     */
    public List<User> getUsers(String query,int page,int limit){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int start = page * limit - limit;
        List<User>users=new ArrayList<>();
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1,start);
            pstmt.setInt(2,limit);
            rs = pstmt.executeQuery();
            while (rs.next())
                users.add(userMapper.mapRow(rs));
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return users;
    }

    /**
     * Get users count
     *
     *
     * @return users count
     *
     */



    private static final String SQL_GET_USER_BY_NAME="Select * from users where login=?";

    public User get(String login) {
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_BY_NAME);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = userMapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return user;
    }


    public int getCount(){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int i=0;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USERS_COUNT);
            rs = pstmt.executeQuery();
            if (rs.next())
                i=rs.getInt(1);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return i;
    }

    /**
     * Get user role type by id
     *
     * @param id int value of role
     *
     * @return String value of role
     *
     */
    public String getUserRole(Long id){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        String returned="";
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_ROLE);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                returned=rs.getString(Fields.ROLE_NAME);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return returned;
    }


    /**
     * Get user by his password and login
     *
     * @param login
     *
     * @param password
     *
     * @return User if user exist or null if not
     *
     */
    public User get(String login,String password) {
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER);
            pstmt.setString(1, login);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = userMapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return user;
    }

    /**
     * Insert user
     *
     * @param entity user for insert
     *
     *
     */

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void add(User entity) {
        /*Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_USER);
            int k = 1;
            pstmt.setString(k++,entity.getLogin());
            pstmt.setString(k++, entity.getPassword());
            pstmt.setLong(k++,getId(entity.getRole()));
            pstmt.setString(k,entity.getEmail());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }*/
        jdbcTemplate.update(SQL_INSERT_USER,entity.getLogin(),entity.getPassword(),2,entity.getEmail());
    }


    /**
     * Update user password
     *
     * @param newPassword
     *
     * @param user used id
     *
     */
    public void updateUserPassword(String newPassword,User user){
        Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_USER_PASSWORD);
            pstmt.setString(1,newPassword);
            pstmt.setLong(2,user.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }


    /**
     * Set user role to admin
     *
     *
     * @param user used id
     *
     */
    public void updateUserRole(User user){
        jdbcTemplate.update(SQL_UPDATE_USER_ROLE,user.getId());
    }

    /**
     * Update user money
     *
     * @param money
     *
     * @param user used id
     *
     */
    public void updateUserMoney(User user, BigDecimal money){
        jdbcTemplate.update(SQL_UPDATE_USER_MONEY,money,user.getId());
    }

    /**
     * Get user by his id
     *
     * @param id
     *
     * @return User
     *
     */
    public User getByID(Long id){
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = userMapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return user;
    }

    /**
     * Get know about existing user with login
     *
     * @param login
     *
     * @return true if exist and false if not
     *
     */
    public boolean getByLogin(String login){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next())
                return true;
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return false;
    }

    /**
     * Get know about existing user with email
     *
     * @param email
     *
     * @return true if exist and false if not
     *
     */
    public boolean getByEmail(String email){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_USER_BY_EMAIL);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery();
            if (rs.next())
                return true;
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return false;
    }


    /**
     * Update user email value
     *
     * @param email
     *
     * @param user used id
     *
     */
    public void updateUserEmail(String email,User user){
        Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_USER_EMAIL);
            pstmt.setString(1,email);
            pstmt.setLong(2,user.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

}
