package com.epam.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * DB manager. Works with MySQl data base!.
 * Only the required DAO methods are defined!
 *
 * @author Kaliuha Aleksei
 *
 */
public class DBManager {

    private static DBManager instance;

    private DBManager() { }

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }


    /**
     * Commits and close the given connection.
     *
     * @param con
     *            Connection to be committed and closed.
     */
    public static void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con
     *            Connection to be rollbacked and closed.
     */
    public static void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return A DB connection.
     */
    public Connection getConnection() throws SQLException {
        try {
            Context ctx = new InitialContext();
            Context envContext  = (Context)ctx.lookup("java:/comp/env");
            DataSource ds = (DataSource)envContext.lookup("jdbc/epam_final_project_data_base");
            return ds.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
            throw new SQLException("rethrow");
        }
    }

    /**DOESN`T USED IN THE PROJECT
     * Returns a DB connection from DRIVER MANAGER
     *
     * @return A DB connection.
     */
    public Connection getConnectionByDriverManager() throws SQLException {
        Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/epam_final_project_data_base?user=root&password=root");
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        connection.setAutoCommit(false);
        return connection;
    }

}
