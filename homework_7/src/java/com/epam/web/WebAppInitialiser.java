package com.epam.web;

import com.epam.web.config.WebConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;


public class WebAppInitialiser extends AbstractAnnotationConfigDispatcherServletInitializer {

	final String location = System.getProperty("java.io.tmpdir");
	private int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext root =
				new AnnotationConfigWebApplicationContext();

		root.scan("com.epam");
		servletContext.addListener(new ContextLoaderListener(root));

		ServletRegistration.Dynamic appServlet =
				servletContext.addServlet("mvc", new DispatcherServlet(new GenericWebApplicationContext()));
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/");
		appServlet.setInitParameter("throwExceptionIfNoHandlerFound", "true");
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(location,
				MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);
		appServlet.setMultipartConfig(multipartConfigElement);
        servletContext.setInitParameter("javax.servlet.jsp.jstl.fmt.localizationContext","resources");
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { WebConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}


}
