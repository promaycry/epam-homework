package com.epam.web.config;

import com.epam.web.handler.FailureHandler;
import com.epam.web.handler.SuccesLoginHandler;
import com.epam.web.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailService userDetailService;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService).
				passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean
	public SessionRegistry sessionRegistry() {
		SessionRegistry sessionRegistry = new SessionRegistryImpl();
		return sessionRegistry;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().
				authorizeRequests()
			.antMatchers("/","/expositionList*","/setting").permitAll()
				.antMatchers("/registerPage","/register","/login").anonymous().
				antMatchers("/orders*","/buy","/deleteOrder","/money").hasRole("USER")
				.antMatchers("/users*","/update","/delete","/add","/addPage").hasRole("ADMIN")
				.antMatchers("/logout").hasAnyRole("USER","ADMIN")
			.and()
				.formLogin().loginProcessingUrl("/login")
				.failureHandler(failureHandler)
					.usernameParameter("login")
				.passwordParameter("password")
				.successHandler(succesLoginHandler)
			.and()
				.logout()
				.logoutUrl("/logout")
			.and()
				.exceptionHandling()
				.accessDeniedPage("/expositionList");
	}

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	SuccesLoginHandler succesLoginHandler;

	@Autowired
	FailureHandler failureHandler;

}