package com.epam.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;

@Aspect
@Component
@EnableAspectJAutoProxy
public class TimeLoger
{
    @Around("@annotation(com.epam.annotation.Time)")
    public Object log(ProceedingJoinPoint point) throws Throwable
    {
        long start = System.currentTimeMillis();
        Object result = point.proceed();
        Logger log = Logger.getLogger(MethodSignature.class.cast(point.getSignature()).getDeclaringTypeName());
        log.trace("methodName= "
                + ((MethodSignature) point.getSignature()).getMethod().getName()+
                "  , execution time= "+
                (System.currentTimeMillis() - start ));
        return result;
    }
}
