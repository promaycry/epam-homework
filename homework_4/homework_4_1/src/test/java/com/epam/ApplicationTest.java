package com.epam;

import com.epam.model.Order;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class, BatchTestConfiguration.class})
public class ApplicationTest {

    @Autowired
    Application.Get get;


    @Test
    public void testIntegration(){
        List<Order> orderList=get.get(new File("file.csv"));
        Assert.assertNotNull(orderList);
        Assert.assertEquals(4,orderList.size());
    }


    @Test
    public void testReadFile(){
        List<String> strings=new Application().readFile(new File("file.csv"));
        Assert.assertNotNull(strings);
        Assert.assertEquals(6,strings.size());
    }


}

