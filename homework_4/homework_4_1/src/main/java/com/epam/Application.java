package com.epam;

import com.epam.integration.aggregator.OrderComponentsReleaseStrategy;
import com.epam.integration.splitter.OrderComponentsSplitter;
import com.epam.model.Order;
import com.epam.model.OrderState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@SpringBootApplication
@Configuration
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        File file=new File("file.csv");
        List<Order>orders=ctx.getBean(Get.class).get(file);
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(ctx.getBean(Get.class).get(file).toString());
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(ctx.getBean(Get.class).get(file).toString());
            }
        }).start();
        log.info("orders --->"+orders);
        log.info("Finished");
        List<Order>orders1=ctx.getBean(Get.class).get(file);
        log.info("orders --->"+orders1);
        log.info("Finished");
        ctx.close();
    }

    @MessagingGateway
    public interface Get {

        @Gateway(requestChannel = "input")
        List<Order> get(File file);

    }

    public List<String>readFile(File file){
        log.info("File name---> "+file.toString());
        List<String>strings=new ArrayList<>();
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String line=myReader.nextLine();
                log.info("line--->  "+line);
                strings.add(line+System.lineSeparator());
            }
            myReader.close();
            log.info("strings--> "+strings.size());
            return strings;
        } catch ( FileNotFoundException e) {
            log.info(e.toString());
            log.info("strings--> "+strings.size());
            return strings;
        }
    }


    public List<Order> getOrder(List<String>strings){
        List<Order>orders=new ArrayList<>();
        for(String string:strings){
        Order order=new Order();
        Matcher m = pattern.matcher(string);
            if(m.find()){
                if(!m.group(2).isEmpty()){
                    order.setOrderState(OrderState.valueOf(m.group(2)));
                    order.setId(Integer.valueOf(m.group(1)));
                    log.info("get order ---> "+order);
                    orders.add(order);
                }
            }
        }
        return orders;
    }

    final static Pattern pattern=Pattern.compile("([1-9]{1,});(WAITING_FOR_PAYMENT|PAYMENT_COMPLETED|CANCELED)");

    public boolean accept(Order order){
        log.info("Filtering order: "+order);
        if(order.getOrderState()==(OrderState.CANCELED)){
            log.info("order " + order + " doesn`t pass filter");
            return false;
        }
        log.info("order " + order + " pass filter");
        return true;
    }

    @Bean
    public OrderComponentsReleaseStrategy orderComponentsReleaseStrategy(){
        return new OrderComponentsReleaseStrategy();
    }


    @Bean
    public IntegrationFlow get() {
        return IntegrationFlows.from("input")
                .<File, List<String>>transform(this::readFile).<List<String>,List<Order>>transform(this::getOrder)
                .split(new OrderComponentsSplitter()).filter(this::accept)
                .aggregate(a->a.releaseStrategy(orderComponentsReleaseStrategy()))
                .get();
    }

}
