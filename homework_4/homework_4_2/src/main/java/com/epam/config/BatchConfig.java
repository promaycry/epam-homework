package com.epam.config;

import com.epam.entity.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.step.Processor;
import com.epam.step.Reader;
import com.epam.step.Writer;
import org.springframework.mail.SimpleMailMessage;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Bean
	public Reader reader(){
		return new Reader();
	}

	@Bean
	public Job processJob(Step step) {
		return jobBuilderFactory.get("processJob")
				.incrementer(new RunIdIncrementer())
				.flow(step).end().build();
	}

	@Bean
	public Step orderStep1(Reader reader) {
		return stepBuilderFactory.get("orderStep1").<User, SimpleMailMessage> chunk(1)
				.reader(reader).processor(new Processor())
				.writer(new Writer()).build();
	}



}
