package com.epam.step;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.springframework.batch.item.ItemWriter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class Writer implements ItemWriter<SimpleMailMessage> {

	@Override
	public void write(List<? extends SimpleMailMessage> list) throws Exception {
		JavaMailSenderImpl mailSender=new JavaMailSenderImpl();
		Properties properties=new Properties();
		InputStream input = new FileInputStream("src/main/resources/email.properties");
		properties.load(input);
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername(properties.getProperty("login"));
		mailSender.setPassword(properties.getProperty("password"));
		Properties prop = mailSender.getJavaMailProperties();
		prop.put("mail.transport.protocol", "smtp");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.debug", "true");
		for(SimpleMailMessage message:list){
			if(message!=null)
				mailSender.send(message);
		}
	}

}