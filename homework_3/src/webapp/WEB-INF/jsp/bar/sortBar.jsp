<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
</head>
<body>
<ul class="nav grey lighten-4 py-4 sortable">
    <li >
        <form action="expositionList" method="post" id="Refresh">
            <input type="hidden" name="command" value="expositionList">
            <a onclick="document.getElementById('Refresh').submit()"><fmt:message key="refresh.button.jsp"/> </a>
        </form>
    </li>
    <li >
        <a>&#160; <fmt:message key="topic.label"/> &#160;</a>
    </li>
    <li >
        <form action="expositionList" id="topicUp" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="topicAsc">
            <a class="button" onclick="document.getElementById('topicUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li>
        <form action="expositionList" id="topicDown" method="post">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="topicDec">
            <a class="button" onclick="document.getElementById('topicDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="tickets.label"/> &#160;</a>
    </li>
    <li >
        <form action="expositionList" id="TicketsUp" method="post">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="ticketsAsc">
            <a class="button" onclick="document.getElementById('TicketsUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="expositionList" id="TicketsDown" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="ticketsDec">
            <a class="button" onclick="document.getElementById('TicketsDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="cost.label"/> &#160;</a>
    </li>
    <li >
        <form action="expositionList" id="CostDown" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="costDec">
            <a class="button" onclick="document.getElementById('CostDown').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="expositionList" id="CostUp" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="costAsc">
            <a class="button" onclick="document.getElementById('CostUp').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a > &#160;<fmt:message key="date.label"/> &#160;</a>
    </li>
    <li >
        <form action="expositionList" id="DateDown" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="dateDec">
            <a class="button" onclick="document.getElementById('DateDown').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="expositionList" id="DateUp" method="post">
            <input type="hidden" name="command" value="expositionList">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="sortBy" value="dateAsc">
            <a class="button" onclick="document.getElementById('DateUp').submit()">
                &#8595;
            </a>
        </form>
    </li>
</ul>
</body>
</html>

