package com.epam.entity;


import java.io.Serializable;

/**
 * Basic common parent for all entities. Provides id field and get/set methods
 * to him.
 *
 * @author Kaliuha Aleksei
 *
 */

public abstract class   Entity implements Serializable {

    private static final long serialVersionUID = 8466257860808346236L;

    private Long id;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

}
