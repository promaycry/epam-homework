package com.epam.util;

import com.epam.annotation.Time;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hash password entity
 *
 * @author Kaliuha Aleksei
 *
 */
public final class Hash {

    private static final Logger log = Logger.getLogger(Hash.class);

    private Hash(){
    }

    private static final String algorithm="MD5";

    /**
     * Hash password
     * @param password
     *
     * @return hashedPassword
     *
     * @author Kaliuha Aleksei
     *
     */
    public static String hash(String password) {
        log.debug("Hash started");
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(password.getBytes());
        StringBuilder hexString= new StringBuilder();
        for (byte Byte:digest.digest()) {
            String hex = Integer.toHexString(0xff & Byte);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString().toUpperCase();
    }

}
