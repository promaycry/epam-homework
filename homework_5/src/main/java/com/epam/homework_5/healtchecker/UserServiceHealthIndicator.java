package com.epam.homework_5.healtchecker;

import com.epam.homework_5.Homework5Application;
import com.epam.homework_5.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.net.Socket;

@Component
@Slf4j
public class UserServiceHealthIndicator implements HealthIndicator {

    private static final String URL="http://localhost:8080/users";

    @Override
    public Health health() {
        try (Socket socket =
                     new Socket(new java.net.URL(URL).getHost(),8080)) {
        } catch (Exception e) {
            log.warn("Failed to connect to: {}",URL);
            return Health.down()
                    .withDetail("name", UserService.class.toString())
                    .withDetail("error", e.getMessage())
                    .build();
        }
        return Health.up().withDetail("name", UserService.class.toString()).build();
    }
}
