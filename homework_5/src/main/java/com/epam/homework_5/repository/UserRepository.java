package com.epam.homework_5.repository;

import com.epam.homework_5.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User,Long> {

    List<User>findAll();

    User getById(Long id);

}
