package com.epam.homework_5.controller;


import com.epam.homework_5.model.User;
import com.epam.homework_5.service.UserService;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@Timed
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/users")
    @ResponseBody
    public List<User> get(@RequestParam(defaultValue = "0") Integer pageNo,
                          @RequestParam(defaultValue = "3") Integer pageSize,
                          @RequestParam(defaultValue = "null") String sortBy){
        log.info("get users request invoke");
        return userService.getAll(pageNo,pageSize,sortBy);
    }

    @GetMapping(value = "/user/{id}")
    @ResponseBody
    public User get(@PathVariable Long id){
        log.info("get user request invoke");
        return userService.get(id);
    }

}
