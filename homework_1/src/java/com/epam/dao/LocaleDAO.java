package com.epam.dao;


import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.dao.DBManager.commitAndClose;
import static com.epam.dao.DBManager.rollbackAndClose;


/**
 * Data access object for locale entity.
 */

@Repository
public class LocaleDAO {

    private static final String SQL_GET_LOCALE_ID="Select id from locales where name=?";

    private static final String SQL_GET_LOCALES_NAMES="Select name from locales";

    private static final String SQL_ADD_EXPOSITION_NAME="Insert into exposition_names(topic," +
            "locale_id,exposition_id) values(?,?,?)";

    /**
     * Returns locale id.
     *
     * @param locale name of locale
     *
     * @return id of locale in database.
     */
    public int getIdLocale(String locale){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int i=0;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_LOCALE_ID);
            pstmt.setString(1,locale);
            rs = pstmt.executeQuery();
            if (rs.next()){
                i=rs.getInt(Fields.ID);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return i;
    }


    /**
     * Returns all locales
     *
     * @return all locales.
     */
    public static List<String> getLocales(){
        List<String>locales=new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_LOCALES_NAMES);
            rs = pstmt.executeQuery();
            while (rs.next()){
                locales.add(rs.getString(Fields.NAME));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return locales;
    }


    /**
     * Insert and connect localize topic to exposition
     *
     * @param id id of exposition
     *
     * @param locale
     *
     * @param topic locale topic
     *
     */
    public void insertLocaleTopic(int id,String topic,String locale) throws SQLException {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_ADD_EXPOSITION_NAME);
            pstmt.setString(1,topic);
            pstmt.setInt(2,new LocaleDAO().getIdLocale(locale));
            pstmt.setInt(3,id);
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new SQLException("return");
        }
        finally {
            DBManager.commitAndClose(con);
        }
    }

}
