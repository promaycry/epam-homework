package com.epam.web.controller;

import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class SettingController {


    @Autowired
    SettingService settingService;

    private static final long serialVersionUID = 7738791314029476456L;

    @RequestMapping(value = "/setting")
    public String setting(HttpServletRequest request, HttpServletResponse response){
        return settingService.setting(request,response);
    }



}
