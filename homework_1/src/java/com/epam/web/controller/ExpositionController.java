package com.epam.web.controller;


import com.epam.web.service.ExpositionService;
import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class ExpositionController {

    @Autowired
    ExpositionService expositionService;

    @Autowired
    SettingService settingService;


    @RequestMapping(value = "/expositionList", method = {RequestMethod.GET, RequestMethod.POST})
    public String expositionList(@RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                                 Model model,
                                 @RequestParam(name = "sortBy",defaultValue = "default")String orderBy,
                                 HttpSession session,
                                 HttpServletRequest request, HttpServletResponse response){
        settingService.setLocale(response,request,session);
        return expositionService.get(session,currentPage,orderBy,model);
    }


    @RequestMapping(value = "/add")
    public String addExposition(HttpServletRequest request, HttpServletResponse response,@RequestParam("image") MultipartFile file) throws IOException {
        return expositionService.add(response,request,file);
    }


    @RequestMapping(value = "/delete")
    public String deleteExposition(@RequestParam(name = "sortBy",defaultValue = "default")String orderBy,
                                   @RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                                   @RequestParam(name="exposition_id") int id
                                   ){
        return expositionService.deleteExposition(id,currentPage,orderBy);
    }

}
