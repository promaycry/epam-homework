<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 14.10.2020
  Time: 19:13
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="WEB-INF/jsp/css/loginPageStyle.css">
</head>
<body>
<div id="id01" class="modal">
    <form class="modal-content animate" id="login_form" action="logIn" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>
        <div class="container">
            <label><b><fmt:message key="login.label"/></b></label>
            <input type="text" placeholder="<fmt:message key="enterLogin"/>" name="login" required>
            <label><b><fmt:message key="password.label"/></b></label>
            <input type="password" placeholder="<fmt:message key="enterPassword"/>" name="password" required>
            <button type="submit"><fmt:message key="signIn.button.jsp" /></button>
        </div>
    </form>
</div>
</body>
</html>
