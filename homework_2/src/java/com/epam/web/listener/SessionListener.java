package com.epam.web.listener;


import com.epam.entity.User;
import com.epam.web.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;



/**
 * Session listener.
 *
 * @author Kaliuha Aleksei
 *
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    private static final Logger log = Logger.getLogger(SessionListener.class);

    private static final int INTERVAl=15*60;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        log.debug("session creating start");
        se.getSession().setMaxInactiveInterval(INTERVAl);
        log.trace("session timeout set to 15 minutes");
        log.debug("session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        log.debug("session destroyed start");
        User user=(User)se.getSession().getAttribute("user");
        if(user!=null){
            UserService.delete(user);
        }
        log.debug("session destroyed");
    }

}
