package com.epam.web.service;

import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.util.Hash;
import com.epam.util.UserQuery;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService extends MyService {

    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(UserService.class);

    public   static  final List<User> loginedUsers= Collections.synchronizedList(new ArrayList<>());

    private static final int PER_PAGE=6;

    @Autowired
    UserDAO userDAO;

    public static synchronized boolean add(User user){
        if(loginedUsers.contains(user)){
            return false;
        }
        loginedUsers.add(user);
        return true;
    }

    public static final synchronized void delete(User user){
        if(loginedUsers.contains(user)){
            loginedUsers.remove(user);
        }
    }

    public String logIn(String password, String login, Model model, HttpSession session){
        password= Hash.hash(password);
        // error handler
        String errorMessage = null;

        if (login.isEmpty() || password.isEmpty()) {
            errorMessage = "loginPassword.empty";
            model.addAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return "jsp/view/errorPage";
        }

        User user = new UserDAO().get(login,password);
        log.trace("Found in DB: user --> " + user);

        if (user == null ) {
            errorMessage = "user.empty";
            model.addAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return "jsp/view/errorPage";
        }
        else {
            if(add(user)){
                session.setAttribute("user", user);
                log.trace("Set the session attribute: user --> " + user);
            }
            else{
                errorMessage = "user.logined";
                model.addAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return "jsp/view/errorPage";
            }
        }

        return "redirect:/expositionList";
    }

    public String logOut(HttpSession session){
        if (session != null){
            User user=(User)session.getAttribute("user");
            delete(user);
            session.removeAttribute("user");
            log.trace("Session user in deleted");
        }

        return "redirect:/expositionList";
    }

    public String register(String email,String password,String login,String passwordConfirm,Model model){

        if(email.isEmpty()||password.isEmpty()||passwordConfirm.isEmpty()||login.isEmpty()){
            String error="email.login.password.empty";
            model.addAttribute("errorMessage", error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        if(!passwordConfirm.equals(password)){
            String error="password.confirm";
            model.addAttribute("errorMessage", error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        if(!email.matches("[a-z0-9]+@[a-z0-9]+[.](com)||(ua)||(ru)||(net)||(org)")){
            String error="email.invalid";
            model.addAttribute("errorMessage", error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        if(new UserDAO().getByLogin(login)||new UserDAO().getByEmail(email)){
            String error="email.login.exist";
            model.addAttribute("errorMessage", error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        User user=new User();
        user.setEmail(email);
        user.setLogin(login);
        user.setPassword(Hash.hash(password));
        user.setRole("authorized user");

        userDAO.add(user);

        user=userDAO.get(user.getLogin(),user.getPassword());

        log.trace("user add--->"+user);

        return "redirect:/expositionList";
    }

    public String get(@RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                      @RequestParam(name = "orderBy",defaultValue = "default") String orderBy,
                      Model model){
        log.debug("Command starts");

        int count=userDAO.getCount();
        log.trace("count of user-->"+count);
        int numberOfPages=count/ PER_PAGE;

        if(count% PER_PAGE !=0){
            numberOfPages++;
        }
        log.trace("number of pages-->"+numberOfPages);
        log.trace("current page-->"+currentPage);
        log.trace("order by-->"+orderBy);
        String query= UserQuery.get(orderBy);

        log.trace("query-->"+query);

        List<User> users=userDAO.getUsers(query,currentPage,PER_PAGE);
        log.trace("get users-->"+users);

        model.addAttribute("noOfPages",numberOfPages);
        log.trace("set noOfPages-->"+numberOfPages);

        model.addAttribute("currentPage",currentPage);
        log.trace("set currentPage-->"+currentPage);

        model.addAttribute("orderBy",orderBy);
        log.trace("set orderBy-->"+orderBy);

        model.addAttribute("userList",users);
        log.trace("set user-->"+users);

        model.addAttribute("currentView","users");
        log.trace("set currentView-->users");

        return "jsp/view/listOfUsers";
    }


    public String updateRole(long id, int currentPage, String orderBy){
        log.debug("Command start");

        log.trace("user id--->"+id);

        User user=new User();

        user.setId(id);

        userDAO.updateUserRole(user);
        log.debug("user with id+"+user.getId()+" updated");


        log.trace("order by--->"+orderBy);


        log.trace("current page--->"+currentPage);

        return"redirect:/users?orderBy="+orderBy+"&currentPage="+currentPage;
    }

    public String updateMoney(String money,
                              String numbercard,
                              String cvv,
                              String userName,
                              String mm,
                              String yy,
                              String currentView,
                              String currentPage,
                              String orderBy,
                              String sortBy,
                              HttpSession session ,
                              Model model){
        log.debug("Command buyExposition starts");

        log.trace("money-->" + money);

        if (money.isEmpty() || !money.matches("[1-9][0-9]{0,}[.]{0,1}[0-9]{0,2}")) {
            String error = "sumInvalid";
            model.addAttribute("errorMessage", error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }

        log.trace("card number-->" + numbercard);

        if (numbercard.isEmpty() || !numbercard.matches("[0-9]{16}")) {
            String error = "cardNumberInvalid";
            model.addAttribute("errorMessage", error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }

        log.trace("CVV-->" + cvv);

        if (cvv.isEmpty() || !cvv.matches("[0-9]{3}")) {
            String error = "cardCVVInvalid";
            model.addAttribute("errorMessage", error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }

        log.trace("User name-->" + userName);
        if (userName.isEmpty()) {
            String error = "cardNameInvalid";
            model.addAttribute("errorMessage", error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }

        log.trace("Month of dissapear-->" + mm);
        log.trace("Year of dissapear-->" + yy);

        if (mm.isEmpty() || yy.isEmpty()) {
            String error = "cardMMYYInvalid";
            model.addAttribute("errorMessage", error);
            log.error("errorMessage --> " + error);
            return "jsp/view/errorPage";
        }
        User user = (User) session.getAttribute("user");
        log.trace("Session user get-->" + user);

        userDAO.updateUserMoney(user, new BigDecimal(money));
        log.debug("User money updated");

        BigDecimal addMoney = new BigDecimal(money);
        addMoney.add(user.getMoney());

        log.trace("Session user set-->" + user);
        session.setAttribute("user", user);

        log.trace("currrent view-->" + currentView);

        if (currentView.equals("expositions")) {
            log.debug("redirect to expositions page");
            return "redirect:/expositionList?&currentPage=" + currentPage + "&sortBy=" + sortBy;
        }

        log.debug("redirect to orders page");
        return "redirect:/orders?currentPage=" + currentPage + "&orderBy=" + orderBy;
    }


}
