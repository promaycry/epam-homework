package com.epam.web.controller;


import com.epam.annotation.Time;
import com.epam.web.service.SettingService;
import com.epam.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    SettingService settingService;

    @Time
    @RequestMapping (value = "/logIn", method = {RequestMethod.POST})
    public String logIn(@RequestParam(name = "login")String login,
                        @RequestParam(name = "password") String password,
                        Model model,
                        HttpSession session){
        return userService.logIn(password,login,model,session);
    }

    @Time
    @RequestMapping (value = "/logOut", method = {RequestMethod.GET, RequestMethod.POST})
    public String logOut(HttpSession session){
        return userService.logOut(session);
    }

    @Time
    @RequestMapping(value = "/register", method = {RequestMethod.POST})
    public String register(@RequestParam(name = "login")String login,
                        @RequestParam(name = "password") String password,
                           @RequestParam(name = "email") String email,
                           @RequestParam(name = "password_confirm") String confirm,
                        Model model){
        return userService.register(email,password,login,confirm,model);
    }

    @Time
    @RequestMapping(value = "/users", method = {RequestMethod.POST,RequestMethod.GET})
    public String get(@RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                      @RequestParam(name = "orderBy",defaultValue = "default") String orderBy,
                      Model model,
                      HttpServletRequest request,
                      HttpServletResponse response,
                      HttpSession session
                      ){
        settingService.setLocale(response,request,session);
        return userService.get(currentPage,orderBy,model);
    }

    @Time
    @RequestMapping(value = "/update",method = {RequestMethod.POST})
    public String update(@RequestParam(name = "currentPage",defaultValue = "1")int currentPage,
                      @RequestParam(name = "orderBy",defaultValue = "default") String orderBy,
                         @RequestParam(name = "user",defaultValue = "default") long user){
        return userService.updateRole(user,currentPage,orderBy);
    }

    @Time
    @RequestMapping(value = "/money",method = {RequestMethod.POST})
    public String update(HttpSession session,
                         Model model,
                         @RequestParam(name = "currentPage",defaultValue = "1")String currentPage,
                         @RequestParam(name = "orderBy",defaultValue = "default") String orderBy,
                         @RequestParam(name = "sortBy",defaultValue = "default") String sortBy,
                         @RequestParam(name = "money") String money,
                         @RequestParam(name = "numberCard") String card,
                         @RequestParam(name = "userName") String username,
                         @RequestParam(name = "cvv") String cvv,
                         @RequestParam(name = "MM") String mm,
                         @RequestParam(name = "YY") String yy,
                         @RequestParam(name = "currentView") String currentView
                         ){
        return userService.updateMoney(money,card,cvv, username,mm,yy,
                currentView,currentPage,orderBy,sortBy,
                session,model);
    }

}
