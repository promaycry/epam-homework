package com.epam.homework_6_2.service;

import com.epam.homework_6_2.annotation.Time;
import com.epam.homework_6_2.model.User;
import com.epam.homework_6_2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Time
    public List<User> getUsers(){
        return userRepository.findAll();
    }

}
