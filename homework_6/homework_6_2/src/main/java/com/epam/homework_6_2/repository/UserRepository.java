package com.epam.homework_6_2.repository;

import com.epam.homework_6_2.annotation.Time;
import com.epam.homework_6_2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    List<User>findAll();

}
