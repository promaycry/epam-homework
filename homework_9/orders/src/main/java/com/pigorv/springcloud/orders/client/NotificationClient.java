package com.pigorv.springcloud.orders.client;

import com.pigorv.springcloud.orders.entity.NotificationDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "notifications", fallback = NotificationClient.NotificationClientImpl.class)
public interface NotificationClient {

    @PostMapping
    public NotificationDto notify(@RequestParam String user);

    @Component
    class NotificationClientImpl implements NotificationClient{

        @Override
        public NotificationDto notify(String user) {
            return new NotificationDto();
        }

    }

}
