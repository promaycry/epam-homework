package com.pigorv.springcloud.orders.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NotificationDto {

    String user;

    Notifier notifyBy = Notifier.EMAIL;

    enum Notifier {
        EMAIL
    }

}
