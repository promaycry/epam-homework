package com.pigorv.springcloud.orders.entity;

import lombok.Data;

@Data
public class UserDto {
    private String name;
}
